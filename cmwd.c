#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// https://conoha.mikumo.com/wp-content/themes/conohamikumo/images/wallpaper/2020_newyear/2560_1440.jpg

int main(void)
{

    char *resoluntion[] = {"2560_1440", "1280_800", "1080_1920", "1242_2688"};
    char *year[] = {"2015", "2016", "2017", "2018", "2019", "2020"};
    char *label[] = {"spring", "summer", "autumn", "winter", "halloween", \
        "christmas", "xmas", "newyear", "swim", "rain", "hiyoko", "comic", "pop", \
        "osc", "skiing", "yukata"};
    
    int i, j, k;
    
    char mkdir[250];
    char mv[250];
    char wget_url[250];
    char filename[38];
    char newfilename[46];

    //system("mkdir -p /home/$USER/Pictures/mikumo/2560_1440/");
    for(i = 0; i < (sizeof(resoluntion) / 8); i++) {

        strcat(strcpy(mkdir, "mkdir -p /home/$USER/Pictures/mikumo/"), resoluntion[i]);
        system(mkdir);

        for(j = 0; j < (sizeof(year) / 8); j++) {

            for(k = 0; k < (sizeof(label) / 8); k++) {

                strcat(strcpy(filename, resoluntion[i]), ".jpg");
                strcat(strcat(strcpy(newfilename, year[j]), "_"), label[k]);

                strcat(strcat(strcat(strcpy( \
                    wget_url, "wget https://conoha.mikumo.com/wp-content/themes/conohamikumo/images/wallpaper/"), \
                    newfilename), "/"), filename);
                system(wget_url);

                strcat(newfilename, ".jpg");

                // mv 2560_1440.jpg /home/$USER/Pictures/mikumo/2560_1440/**.jpg
                strcat(strcat(strcat(strcat(strcat(strcpy(mv, "mv "), filename), \
                    " /home/$USER/Pictures/mikumo/"), resoluntion[i]), "/"), newfilename);
                system(mv);
            }
        }
    }

    return 0;
}